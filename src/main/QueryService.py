from src.main.Database import Database

class QueryService:
    def get_all_companies(self):
        """
        Get all companies from analyse database
        :return Tuple List:
        """
        # Database object
        db = Database()

        # Select all records from companies
        query = """SELECT * FROM Companies"""
        data = db.get_query(query, True)

        return data

    def get_company(self, company_id):
        """
        Get company by id
        :param company_id:
        :return company:
        """
        # Database object
        db = Database()

        query = f"""SELECT DISTINCT * FROM companies WHERE company_id = {company_id}"""
        data = db.get_query(query, True)

        return data

    def get_recommendation_weights_for_company(self, company_id):
        """
        Get the recommmendation weights for a company
        :param company_id:
        :return:
        """
        # Database object
        db = Database()

        query = f"""SELECT DISTINCT weight FROM recommendations WHERE company_one_id = {company_id} ORDER BY weight"""
        weights = db.get_query(query, True)
        return weights

    def get_locations_of_company(self, company_id):
        """
        Get locations of recommendations
        :param company_id:
        :return recommendations:
        """
        # Database object
        db = Database()

        query = f"""SELECT DISTINCT companies.company_id, recommendations.company_two_id,
                    (SELECT companies.latitude FROM companies WHERE company_id = recommendations.company_two_id) AS 'latitude',
                    (SELECT companies.longitude FROM companies WHERE company_id = recommendations.company_two_id) AS 'longitude',
                    recommendations.weight
                    FROM recommendations 
                    INNER JOIN companies on companies.company_id = recommendations.company_one_id
                    WHERE companies.company_id = {company_id}
                    ORDER BY weight DESC LIMIT 500"""

        recommendations = db.get_query(query, True)
        return recommendations

    def get_recommendatinons_for_company(self, company_id):
        """
        Get the recommendations for a company
        :param company_id:
        :return recommendations:
        """
        # Database object
        db = Database()

        query = f"""SELECT DISTINCT companies.company_id, recommendations.company_two_id,
                    (SELECT companies.company_name FROM companies WHERE company_id = recommendations.company_two_id) AS 'company_name',
                    (SELECT companies.state FROM companies WHERE company_id = recommendations.company_two_id) AS 'state',
                    (SELECT companies.county FROM companies WHERE company_id = recommendations.company_two_id) AS 'county',
                    (SELECT companies.city FROM companies WHERE company_id = recommendations.company_two_id) AS 'city',
                    (SELECT companies.district FROM companies WHERE company_id = recommendations.company_two_id) AS 'district',
                    (SELECT companies.street FROM companies WHERE company_id = recommendations.company_two_id) AS 'street',
                    (SELECT companies.industry FROM companies WHERE company_id = recommendations.company_two_id) AS 'industry',
                    (SELECT companies.latitude FROM companies WHERE company_id = recommendations.company_two_id) AS 'latitude',
                    (SELECT companies.longitude FROM companies WHERE company_id = recommendations.company_two_id) AS 'longitude',
                    recommendations.weight
                    FROM recommendations 
                    INNER JOIN companies on companies.company_id = recommendations.company_one_id
                    WHERE companies.company_id = {company_id}
                    ORDER BY weight DESC LIMIT 500"""
        recommendations = db.get_query(query, True)
        return recommendations

    def insert_into_recommendations(self, values):
        """
        Insert into recommendations
        :return None:
        """
        # Database object
        db = Database()

        deleteQuery = f"""DELETE FROM recommendations WHERE company_one_id = {values[0][0]}"""
        db.execute_query(deleteQuery, True)

        query = """REPLACE INTO Recommendations (company_one_id, company_two_id, weight) VALUES (%s, %s, %s)"""
        db.execute_many_query(query, values, True)

    def get_company_parent_industries(self):
        """
        Get the industry of a company.
        These are categorized industries. This is for a wider check.
        For example:
        IT industry and Biological technology both belong to the wider industry: tech.
        :return list:
        """
        # Database object
        db = Database()

        query = f"""SELECT companies.company_id, companies.industry, Parent_Industry.parent_id, Parent_Industry.parent
                            FROM Parent_Company
                            INNER JOIN companies on Parent_Company.company_id = companies.company_id
                            INNER JOIN Parent_Industry on Parent_Company.parent_id = Parent_Industry.parent_id
                            ORDER BY company_id ASC"""
        data = db.get_query(query, True)
        return data

    def get_all_cities_with_industries(self):
        """
        Get all distinct cities from all companies
        :return cities:
        """
        # Database object
        db = Database()

        query = f"""SELECT companies.company_id, companies.industry, companies.city, Parent_Industry.parent
                    FROM companies 
                    INNER JOIN Parent_Company ON Parent_Company.company_id = companies.company_id
                    INNER JOIN Parent_Industry ON Parent_Industry.parent_id = Parent_Company.parent_id 
                    ORDER BY city ASC
                    """
        data = db.get_query(query, True)
        return data

