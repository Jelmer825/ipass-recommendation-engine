import math
import random
from src.main.Calculator import *
from src.main.QueryService import QueryService
import numpy as np


class Engine:
    def insert_recommendation_table(self,
                                    company_id,
                                    street_multiplier,
                                    district_multiplier,
                                    city_multiplier,
                                    county_multiplier,
                                    state_multiplier,
                                    industry_multiplier,
                                    category_multiplier,
                                    hotspot_multiplier):
        """
        Insert data in recommendation table
        :return None:
        """
        tuple_list = self.recommend(company_id,
                                    street_multiplier,
                                    district_multiplier,
                                    city_multiplier,
                                    county_multiplier,
                                    state_multiplier,
                                    industry_multiplier,
                                    category_multiplier,
                                    hotspot_multiplier)

        print(f"Started inserting with len: {len(tuple_list)}")
        print(f"With the following params:\n"
              f"Company_Id:{company_id}\n"
              f"Street multiplier: {street_multiplier}\n"
              f"District multiplier: {district_multiplier}\n"
              f"City multiplier: {city_multiplier}\n"
              f"County multiplier: {county_multiplier}\n"
              f"State multiplier: {state_multiplier}\n"
              f"Industry multiplier: {industry_multiplier}\n"
              f"Category multiplier: {category_multiplier}\n"
              f"Hotspot multiplier: {hotspot_multiplier}"
              )

        qs = QueryService()
        qs.insert_into_recommendations(tuple_list)

    def recommend(self,
                  company_id,
                  street_multiplier,
                  district_multiplier,
                  city_multiplier,
                  county_multiplier,
                  state_multiplier,
                  industry_multiplier,
                  category_multiplier,
                  hotspot_multiplier):
        """
        For every company i check for company j the distance and calculate
        the recommendation 'strength' based on distance and similarity.
        :return tuple_list:
        """
        qs = QueryService()

        companies = qs.get_all_companies()
        company_industries = qs.get_company_parent_industries()
        index_dict = create_index_dict(company_industries)

        i = qs.get_company(company_id)[0]

        # All hotspots
        hotspots = self.calculate_hotspots()

        # K is for data Normalization
        # The Netherlands extends 312 km (194 mi) N – S and 264 km (164 mi) E – W .
        k = (312 + 264) / 2

        # List for recommendation table
        recommendation_list = []

        # Define the multiplier of i based on industry
        indexes_i = index_dict[i[0]]
        industry_data_i = find_sub_tuple_in_list(company_industries, indexes_i)

        # Random recommendation marker
        rand_marker = random.random()
        scores = []
        for j in companies:
            # Distance between two companies
            distance = haversine(float(i[1]), float(i[2]), float(j[1]), float(j[2]))

            # Define multiplier based on location
            multiplier_hierarchy = calculate_hierarchy_multiplier(i, j,
                                                                  street_multiplier,
                                                                  district_multiplier,
                                                                  city_multiplier,
                                                                  county_multiplier,
                                                                  state_multiplier)

            # Hotspot multiplier
            hotspot_multi = self.check_hotspot(i, j, hotspot_multiplier, hotspots)

            # Define the multiplier of j based on industry
            indexes_j = index_dict[i[0]]
            industry_data_j = find_sub_tuple_in_list(company_industries, indexes_j)
            industry_multi = calculate_industry_multiplier(industry_data_i, industry_data_j, i, j,
                                                           industry_multiplier, category_multiplier)
            # Score
            if distance != 0:
                # Calculate score...
                score = ((multiplier_hierarchy * k / math.sqrt(distance)) * industry_multi) * hotspot_multi
                scores.append(score)
                recommendation_list.append([i[0], j[0], 0])

        normalized_scores = (scores - np.min(scores)) / (np.max(scores) - np.min(scores))
        for x in range(len(normalized_scores)):
            # Mutation randomizer
            mutation = random.random()
            if normalized_scores[x] <= rand_marker or mutation == 0.01:
                recommendation_list[x][2] = normalized_scores[x]
                recommendation_list[x] = tuple(recommendation_list[x])
        return recommendation_list

    def check_hotspot(self, i, j, hotspot_multiplier, hotspots):
        """
        Compare two companies and check if they share a hotspot
        :param i:
        :param j:
        :param hotspot_multiplier:
        :param hotspots:
        :return:
        """
        hotspot_i = hotspots.get(i[5])
        hotspot_j = hotspots.get(j[5])
        if hotspot_i == hotspot_j:
            return hotspot_multiplier
        else:
            return 1

    def calculate_hotspots(self):
        """
        Calculate hotspots
        :return hotspot dict:
        """
        qs = QueryService()

        result = qs.get_all_cities_with_industries()

        indexes = collections.defaultdict(list)
        for i, tup in enumerate(result):
            indexes[tup[2]].append(tup[3])

        city_dict = {}
        for key in indexes:
            d = {}
            for i in range(len(indexes[key]) - 1):
                x = indexes[key][i]
                c = 0
                for j in range(i, len(indexes[key])):
                    if indexes[key][j] == indexes[key][i]:
                        c = c + 1
                count = dict({x: c})
                if x not in d.keys():
                    d.update(count)
            if d:
                max_key = max(d, key=d.get)
                city = dict({key: max_key})
                city_dict.update(city)

        return city_dict
