# README #

### What is this repository for? ###

* This repository is for the recommendation algorithm. 

### Setup ###

* Make a database according to the ERD. *
* Create a .env file 
* Install modules 

#### ENV example ####

* DB_LIVE_HOST=
* DB_LIVE_DATABASE=
* DB_LIVE_USERNAME=
* DB_LIVE_PASSWORD=
* 
* DB_ANALYSE_HOST=
* DB_ANALYSE_DATABASE=
* DB_ANALYSE_USERNAME=
* DB_ANALYSE_PASSWORD=

### Install modules

* pip install mysqlclient
* pip install pdoc3
* pip install python-dotenv

### Run tests
* python Tests.py

### Run program
* python Main.py

### Sources ###

* http://cs.uef.fi/pages/franti/lami/papers/A%20Survey%20on%20Recommendations%20in%20Location-based%20Social%20Networks.pdf
* https://en.wikipedia.org/wiki/Haversine_formula
* https://towardsai.net/p/data-science/how-when-and-why-should-you-normalize-standardize-rescale-your-data-3f083def38ff