from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.main.Engine import Engine
from src.main.QueryService import QueryService

app = FastAPI()

# Define origins which can use this API
origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:5000",
    "http://localhost:8000",
]

# ALLOW CROSS ORIGIN
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/companies')
async def get_companies():
    """
    Get companies from database
    :return json response:
    """
    qs = QueryService()
    companies = qs.get_all_companies()
    return companies

@app.get('/recommendations/')
async def get_recommendations_for_id(companyId: int,
                                     streetMultiplier: float,
                                     districtMultiplier: float,
                                     cityMultiplier: float,
                                     countyMultiplier: float,
                                     stateMultiplier: float,
                                     industryMultiplier: float,
                                     categoryMultiplier: float,
                                     hotspotMultiplier: float):
    """
    Get recommendations
    :param companyId:
    :param streetMultiplier:
    :param districtMultiplier:
    :param cityMultiplier:
    :param countyMultiplier:
    :param stateMultiplier:
    :param industryMultiplier:
    :param categoryMultiplier:
    :param hotspotMultiplier:
    :return recommendations:
    """
    engine = Engine()
    engine.insert_recommendation_table(companyId,
                                       streetMultiplier,
                                       districtMultiplier,
                                       cityMultiplier,
                                       countyMultiplier,
                                       stateMultiplier,
                                       industryMultiplier,
                                       categoryMultiplier,
                                       hotspotMultiplier)
    qs = QueryService()
    recommendations = qs.get_recommendatinons_for_company(companyId)
    return recommendations

@app.get('/recommendations/locations/{companyId}')
async def get_recommendations_for_id(companyId: int):
    qs = QueryService()
    locations = qs.get_locations_of_company(companyId)
    return locations

@app.get('/weights/{company_id}')
async def get_weights(company_id: int):
    """
    Get weights from recommendations
    :return json response:
    """
    qs = QueryService()
    weights = qs.get_recommendation_weights_for_company(company_id)

    list = []
    for weight in weights:
        list.append(weight[0])
    return list

@app.get('/company/{company_id}')
async def get_company(company_id: int):
    """
    Get company from database
    :return json response:
    """

    qs = QueryService()
    company = qs.get_company(company_id)

    return company