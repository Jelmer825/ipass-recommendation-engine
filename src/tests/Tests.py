import unittest
import os

from dotenv import load_dotenv
from src.main.Calculator import *
from src.main.Database import Database
from src.main.QueryService import QueryService

class TestCalculator(unittest.TestCase):
    def test_haversine(self):
        lat1 = 52.0799838
        lon1 = 4.3113461
        lat2 = 52.3727598
        lon2 = 4.8936041
        self.assertEqual(haversine(lat1, lon1, lat2, lon2), 72.42106866835007)

    def test_calculate_multiplier(self):
        company1 = (1, 52, 52, "Zuid-Holland", "Den Haag", "Den Haag", "Benoordenhout", "Wassenaarseweg", "oil", 124)
        company2 = (2, 52, 52, "Zuid-Holland", "Den Haag", "Den Haag", "Benoordenhout", "Wassenaarseweg", "oil", 50)
        self.assertEqual(calculate_multiplier(company1, company2), 10)

    def test_calculate_industry_multiplier(self):
        company1 = (0, 0, 0, 0, 0, 0, 0, 0, "it-services")
        company2 = (0, 0, 0, 0, 0, 0, 0, 0, "it-services")
        data1 = ((0, 0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0, 0, 0, 0))
        data2 = ((0, 0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0, 0, 0, 0))
        self.assertEqual(calculate_industry_multiplier(data1, data2, company1, company2), 4)

    def test_calculate_industry_category_multiplier(self):
        company1 = (1, 2, 3, 4, 5, 6, 7, 8, 9)
        company2 = (9, 8, 7, 6, 5, 4, 3, 2, 1)
        data1 = ((0, 0, "tech", 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0, 0, 0, 0))
        data2 = ((0, 0, 0, 0, 0, 0, 0, 0, 0), (9, 0, "tech", 0, 0, 0, 0, 0, 0))
        self.assertEqual(calculate_industry_multiplier(data1, data2, company1, company2), 2)

class TestQueryService(unittest.TestCase):
    def test_get_all_companies(self):
        qs = QueryService()
        self.assertTrue(qs.get_all_companies())

class TestDatabase(unittest.TestCase):
    def test_connect(self):
        load_dotenv()
        DB_ANALYSE_HOST = os.environ.get('DB_ANALYSE_HOST')
        DB_ANALYSE_DATABASE = os.environ.get('DB_ANALYSE_DATABASE')
        DB_ANALYSE_USERNAME = os.environ.get('DB_ANALYSE_USERNAME')
        DB_ANALYSE_PASSWORD = os.environ.get('DB_ANALYSE_PASSWORD')
        db = Database()

        self.assertTrue(db.connect(DB_ANALYSE_HOST,
                                   DB_ANALYSE_DATABASE,
                                   DB_ANALYSE_USERNAME,
                                   DB_ANALYSE_PASSWORD))
if __name__ == '__main__':
    unittest.main()