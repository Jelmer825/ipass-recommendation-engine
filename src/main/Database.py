import os
from dotenv import load_dotenv
import MySQLdb


class Database:
    def __init__(self):
        # Load .env
        load_dotenv()

        # Initialise environment variables
        self.DB_LIVE_HOST = os.getenv('DB_LIVE_HOST')
        self.DB_LIVE_DATABASE = os.getenv('DB_LIVE_DATABASE')
        self.DB_LIVE_USERNAME = os.getenv('DB_LIVE_USERNAME')
        self.DB_LIVE_PASSWORD = os.getenv('DB_LIVE_PASSWORD')

        self.DB_ANALYSE_HOST = os.getenv('DB_ANALYSE_HOST')
        self.DB_ANALYSE_DATABASE = os.getenv('DB_ANALYSE_DATABASE')
        self.DB_ANALYSE_USERNAME = os.getenv('DB_ANALYSE_USERNAME')
        self.DB_ANALYSE_PASSWORD = os.getenv('DB_ANALYSE_PASSWORD')

    def connect(self, host, database, username, password):
        """
        Connect with database
        :return cursor:
        """
        try:
            db = MySQLdb.connect(
                host=host,
                database=database,
                user=username,
                password=password,
            )
            return db
        except:
            print("Can't connect to database")
            return None

    def execute_query(self, query, analyse):
        """
        Executes incoming query with SINGLE data
        :param analyse:
        :param query:
        :return None:
        """

        # Database connection
        if analyse:
            db = self.connect(self.DB_ANALYSE_HOST,
                              self.DB_ANALYSE_DATABASE,
                              self.DB_ANALYSE_USERNAME,
                              self.DB_ANALYSE_PASSWORD)
        else:
            db = self.connect(self.DB_LIVE_HOST,
                              self.DB_LIVE_DATABASE,
                              self.DB_LIVE_USERNAME,
                              self.DB_LIVE_PASSWORD)

        if db:
            # Database cursor
            cur = db.cursor()

            # Execute and commit query
            cur.execute(query)
            db.commit()
            db.close()

    def execute_many_query(self, query, values, analyse):
        """
        Executes incoming query with MANY data
        :param values:
        :param analyse:
        :param query:
        :return None:
        """

        # Database connection
        if analyse:
            db = self.connect(self.DB_ANALYSE_HOST,
                              self.DB_ANALYSE_DATABASE,
                              self.DB_ANALYSE_USERNAME,
                              self.DB_ANALYSE_PASSWORD)
        else:
            db = self.connect(self.DB_LIVE_HOST,
                              self.DB_LIVE_DATABASE,
                              self.DB_LIVE_USERNAME,
                              self.DB_LIVE_PASSWORD)

        if db:
            # Database cursor
            cur = db.cursor()

            # Execute and commit query
            cur.executemany(query, values)
            db.commit()
            db.close()
            print("Done!")

    def get_query(self, query, analyse):
        """
        Get data from database
        :param query:
        :param analyse:
        :return Tuple list:
        """
        # Database connection
        if analyse:
            db = self.connect(self.DB_ANALYSE_HOST,
                              self.DB_ANALYSE_DATABASE,
                              self.DB_ANALYSE_USERNAME,
                              self.DB_ANALYSE_PASSWORD)
        else:
            db = self.connect(self.DB_LIVE_HOST,
                              self.DB_LIVE_DATABASE,
                              self.DB_LIVE_USERNAME,
                              self.DB_LIVE_PASSWORD)

        # Database cursor
        cur = db.cursor()

        # Execute query and fetch data
        cur.execute(query)
        rows = cur.fetchall()
        db.close()
        return rows
