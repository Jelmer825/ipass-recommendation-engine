import collections
import math as m

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth
    :param lon1:
    :param lat1:
    :param lon2:
    :param lat2:
    :return Float:
    """

    # Convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(m.radians, [lon1, lat1, lon2, lat2])

    # Haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = m.sin(dlat / 2) ** 2 + m.cos(lat1) * m.cos(lat2) * m.sin(dlon / 2) ** 2
    c = 2 * m.asin(m.sqrt(a))
    r = 6371  # Radius of earth in kilometers
    return c * r

def calculate_hierarchy_multiplier(company1, company2,
                                   street_multiplier=10,
                                   district_multiplier=8,
                                   city_multiplier=6,
                                   county_multiplier=4,
                                   state_multiplier=2):
    """
    Calculate the multiplier of a relation
    Multiplier 10: if the companies share a street
    Multiplier 8: if the companies share a district
    Multiplier 6: if the companies share a city
    Multiplier 4: if the companies share a county
    Multiplier 2: if the companies share a state
    :param city_multiplier:
    :param state_multiplier:
    :param county_multiplier:
    :param district_multiplier:
    :param street_multiplier:
    :param company1:
    :param company2:
    :return Integer:
    """
    # If companies share the same street and same city. The city check is there because some street names are also
    # In other cities.
    if company1[7] == company2[7] and company1[5] == company2[5] and company1[7] and company2[7]:
        return street_multiplier
    # If companies share the same district
    elif company1[6] == company2[6] and company1[6] and company2[6]:
        return district_multiplier
    # If companies share the same city
    elif company1[5] == company2[5]:
        return city_multiplier
    # If companies share the same county
    elif company1[4] == company2[4]:
        return county_multiplier
    # If companies share the same state
    elif company1[3] == company2[3]:
        return state_multiplier
    else:
        return 1

def calculate_industry_multiplier(data1, data2, company1, company2, industry_multiplier=4, category_multiplier=2):
    """
    Calculate industry multiplier
    :param category_multiplier:
    :param industry_multiplier:
    :param data1:
    :param data2:
    :param company1:
    :param company2:
    :return:
    """
    # If companies share the same industry
    if company1[8] == company2[8]:
        return industry_multiplier
    else:
        for tup in data1:
            # If companies share the same industry category.
            if any(tup[3] in tup2 for tup2 in data2):
                return category_multiplier
    return 1

def find_sub_tuple_in_list(tuple_dict, indexes):
    """
    Get every tuple from dict for every index
    :param indexes:
    :param tuple_dict:
    :return list:
    """
    temp_list = []
    for x in indexes:
        temp_list.append(tuple_dict[x])
    return temp_list

def create_index_dict(tuple_list):
    """
    Create dictionary for company_id in tuple_list
    :param tuple_list:
    :return index dict:
    """
    indexes = collections.defaultdict(list)
    for i, tup in enumerate(tuple_list):
        indexes[tup[0]].append(i)
    return indexes
